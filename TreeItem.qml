import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    id: root

    property var view
    property var item
    property bool loadChildren: false
    property int depth: 0

    width: parent.width
    implicitHeight: childrenLoader.active ? row.height + childrenLoader.height : row.height

    onLoadChildrenChanged: {
        if (loadChildren) {
            item.getChildren()
        }
    }

    RowLayout  {
        id: row

        x: toggleChildrenButton.width * root.depth

        Item {
            width: toggleChildrenButton.width
            height: 1
            visible: !toggleChildrenButton.visible
        }

        ToolButton {
            id: toggleChildrenButton

            icon.name: childrenLoader.active ? "arrow-down" : "arrow-right"
            visible: root.item.childCount() > 0
            onClicked: {
                switch (childrenLoader.status) {
                case Loader.Null:
                    childrenLoader.active = true
                    break
                case Loader.Ready:
                    childrenLoader.active = false
                    break
                }
            }
        }

        Text {
            text: item.name
        }
    }

    Loader {
        id: childrenLoader

        width: root.width
        anchors.top: row.bottom
        active: false
        sourceComponent: ColumnLayout {
            spacing: 0

            Repeater {
                model: root.item.childCount()
                delegate: Loader {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Component.onCompleted: {
                        const child = root.item.child(modelData)
                        const props = {
                            view: root.view,
                            item: child,
                            loadChildren: true,
                            width: parent.width,
                            depth: root.depth + 1
                        }
                        setSource("TreeItem.qml", props)
                    }
                }
            }
        }
    }
}
