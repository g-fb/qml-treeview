#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStandardPaths>

#include <treeitem.h>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    auto *rootItem = new TreeItem("root");
    QStringList libraries = QStringList() << QStandardPaths::writableLocation(QStandardPaths::HomeLocation)
                                          << QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    for (const auto &library : qAsConst(libraries)) {
        auto item = new TreeItem(library, rootItem);
        item->getChildren();
        rootItem->appendChild(item);
    }

    engine.rootContext()->setContextProperty("treeRootItem", QVariant::fromValue(rootItem));

    engine.load(url);

    return app.exec();
}
