### main.cpp
```c++

#include <treeitem.h>

...

QQmlApplicationEngine engine;

...

auto *rootItem = new TreeItem("root");
// root folders to show in the treeview
QStringList libraries = QStringList() << QStandardPaths::writableLocation(QStandardPaths::HomeLocation)
                                      << QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
for (const auto &library : qAsConst(libraries)) {
    auto item = new TreeItem(library, rootItem);
    item->getChildren();
    rootItem->appendChild(item);
}

engine.rootContext()->setContextProperty("treeRootItem", QVariant::fromValue(rootItem));

...

engine.load(url);
```

main.qml
```qml
ListView {
    id: treeView

    anchors.fill: parent
    model: treeRootItem.childCount()
    clip: true
    delegate: TreeItem {
        view: treeView
        item: treeRootItem.child(modelData)
    }

    function clicked(folder) {
        root.clicked(folder)
    }
}
```
