#include "treeitem.h"

#include <QCollator>
#include <QDirIterator>
#include <QFileInfo>

TreeItem::TreeItem(const QString &path, TreeItem *parent)
    : QObject(parent)
    , m_path(path)
    , m_name(QFileInfo(m_path).fileName())
    , m_parentItem(parent)
{}

void TreeItem::appendChild(TreeItem *item)
{
    m_childItems.append(item);
}

TreeItem *TreeItem::child(int row)
{
    if (row < 0 || row >= m_childItems.size())
        return nullptr;
    return m_childItems.at(row);
}

int TreeItem::childCount() const
{
    return m_childItems.count();
}

TreeItem *TreeItem::parentItem()
{
    return m_parentItem;
}

QString TreeItem::path() const
{
    return m_path;
}

const QString &TreeItem::name() const
{
    return m_name;
}

bool TreeItem::getChildren()
{
    if (childCount() > 0 || !hasChildren()) {
        return false;
    }

    QStringList folders;
    QDirIterator it(path(), QDir::Dirs|QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags);
    while (it.hasNext()) {
        auto folder = it.next();
        folders << folder;
    }

    QCollator collator;
    collator.setNumericMode(true);
    std::sort(folders.begin(), folders.end(), collator);

    for (const auto & folder : qAsConst(folders)) {
        auto item = new TreeItem(folder, parentItem());
        appendChild(item);
    }
    return true;
}

bool TreeItem::hasChildren()
{
    QDirIterator it(path(), QDir::Dirs|QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags);
    return !it.next().isEmpty();
}
