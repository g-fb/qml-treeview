#ifndef TREEITEM_H
#define TREEITEM_H

#include <QObject>
#include <QVariant>

class TreeItem : public QObject
{
    Q_OBJECT
public:
    explicit TreeItem(const QString &path, TreeItem *parent = nullptr);

    void appendChild(TreeItem *child);
    Q_INVOKABLE TreeItem *child(int row);
    TreeItem *parentItem();
    Q_INVOKABLE int childCount() const;
    Q_INVOKABLE bool getChildren();
    Q_INVOKABLE bool hasChildren();

    Q_PROPERTY(QString path READ path CONSTANT)
    Q_INVOKABLE QString path() const;

    Q_PROPERTY(QString name READ name CONSTANT)
    const QString &name() const;

private:
    QString m_path;
    QString m_name;
    TreeItem *m_parentItem;
    QList<TreeItem *> m_childItems;
};

#endif // TREEITEM_H
