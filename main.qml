import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ListView {
        id: treeView

        anchors.fill: parent
        model: treeRootItem.childCount()
        clip: true
        delegate: TreeItem {
            view: treeView
            item: treeRootItem.child(modelData)
        }

        function clicked(folder) {
            root.clicked(folder)
        }
    }
}
